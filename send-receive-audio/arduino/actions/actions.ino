#include <RF24.h>
#include <SPI.h>
#include <RF24Audio.h>
#include "printf.h"     // General includes for radio and audio lib
#include <DS3231.h>
#include <RBD_Timer.h>

#define CE_PIN      7
#define CS_PIN      8
#define RADIO_NUM   1
#define CHANNEL     0

RF24 radio(CE_PIN, CS_PIN);                     // Set radio up using pins 7 (CE) 8 (CS)
RF24Audio rfAudio(radio, RADIO_NUM, CHANNEL);   // Set up the audio using the radio, and set to radio number 1 and set channel to 3

RBD::Timer timer1;
RBD::Timer timer2;

void setup() {      
  Serial.begin(115200);                         // Enable Arduino serial library
  radio.begin();                                // Must start the radio here, only if we want to print debug info
  printf_begin();                               // Radio library uses printf to output debug info  

  //--------------------NRF24L01+ Radio Initializations----------
  radio.setPayloadSize(32);
  radio.setPALevel(RF24_PA_MAX);
  rfAudio.setVolume(7);                         // max vol
  radio.printDetails();                         // Print the info

  //--------------------Protothreading Timer Initializations-----
  timer1.setTimeout(60000);
  timer1.restart();
  
  timer2.setTimeout(80000);
  timer2.restart();

  //--------------------RF24Audio Library Initializations--------
  rfAudio.begin();                              // Start up the radio and audio libararies
}

void loop() {
  if (timer1.onRestart()) {
      Serial.println("Begin Audio Tx");
      audioTransmit();
  }
  if (timer2.onRestart()) {
     Serial.println("End Audio Tx");
     //radio.powerDown();
     rfAudio.receive();
  }
}

void audioTransmit() {
  radio.powerUp();
  rfAudio.transmit();
  Serial.print("Radio Number     = ");
  Serial.println(RADIO_NUM);
  Serial.print("Channel          = ");
  Serial.println(CHANNEL);
}
