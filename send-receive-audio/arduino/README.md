#Arduino Software

This is the software for Listening Post which uses Arduino Uno Board

###Deploying the Software to Arduino Board using Arduino IDE

Download Arduino IDE from this link.
https://www.arduino.cc/en/Main/Software

Install it.

Copy required libraries to Arduino Libraries directory.

In macOS,
>/Users/USER_NAME/Documents/Arduino/libraries

In Windows,
>C:/Users/USER_NAME/Documents/Arduino/libraries

After that,  and open the following file in Arduino IDE
>./actions/actions.ino

Connect the Arduino Board and Upload the sketch.

That's all! Have fun...