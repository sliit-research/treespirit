#include <iostream>
#include <RF24/RF24.h>
#include <string>
#include <sstream>
#include <ctime>
#include "wavfile.h"

using namespace std;

RF24 radio(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);

const uint64_t pipes[14] = { 0xABCDABCD71LL,
							 0x544d52687CLL,
							 0x544d526832LL,
							 0x544d52683CLL,
							 0x544d526846LL,
							 0x544d526850LL,
							 0x544d52685ALL,
							 0x544d526820LL,
							 0x544d52686ELL,
							 0x544d52684BLL,
							 0x544d526841LL,
							 0x544d526855LL,
							 0x544d52685FLL,
							 0x544d526869LL};

int main(int argc, char** argv){
	radio.begin(); 

	radio.setChannel(0);
	radio.setPALevel(RF24_PA_MAX);
	radio.setDataRate(RF24_2MBPS);
	radio.setAutoAck(0);
	radio.setCRCLength(RF24_CRC_8);
	radio.openReadingPipe(1, pipes[1+1]);
	radio.printDetails();
	radio.startListening();

	uint8_t pipe_num = 0;
	int count = 0;
	const int samples = 11040 * 15; // ~ 15 Seconds
	int payloadSize = 32;
	
	uint8_t soundClip[samples];

	clock_t start;
    double duration;

    start = clock();

    if(radio.isChipConnected()){
    	cout << "NRF24L01+ is connected!" << endl;
    } else {
    	cout << "NRF24L01+ is not connected! Terminating..." << endl;
    	return 1;
    }

	while(count < samples) {
		if(radio.available(&pipe_num)) {
			duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;

			switch((int) pipe_num){
				case 1:
					if((count <= samples)){
						cout << "Payload Received from Pipe: " << (int) pipe_num << endl;
						cout << "Time: " << duration << " s" << endl;
						cout << "Sample ID: " << count << "/" << samples << endl;
						cout << "Payload Size: " << payloadSize << endl;
						uint8_t audioData[payloadSize];

						radio.read(&audioData, payloadSize);

						for (int i=0; (i < payloadSize && count < samples); i++) {
							cout << audioData[i];
							soundClip[count++] = audioData[i];
						}

						cout << endl << endl;
					}

					break;

				default:
					cout << "Payload Received from Pipe: " << (int) pipe_num << endl;
					break;
			}
	
		}
	}

	time_t unixTimestamp = time(0);

	stringstream waveFileName;
	waveFileName << "audio_" << unixTimestamp << ".wav";

	FILE * audioFile = wavfile_open(waveFileName.str().c_str());

	if(audioFile) {
		wavfile_write(audioFile, soundClip, samples);
		wavfile_close(audioFile);
		cout << "Audio File Generated: " << waveFileName.str() << endl;
	} else {
		cout << "Couldn't open sound.wav for writing!" << endl;
	}
}
